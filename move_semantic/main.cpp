#include <iostream>
#include <string>

using namespace std;

//string foo(string arg)
//{
//    cout << "string arg" << endl;
//    return arg;
//}

string foo(const string& arg)
{
    cout << "const string& arg" << endl;
    return arg + ", a kot ma ale";
}

string foo(string&& rval_arg)
{
    cout << "string&& arg" << endl;
    rval_arg += ", a kot ma ale";
    return rval_arg;
}

int main()
{
    cout << "Hello World!" << endl;
    std::string my_str("Ala ma kota");
    cout << foo(my_str) << endl;
    cout << foo("Leszek " + my_str) << endl;

    string&& res = foo(std::move(my_str));
    cout << res << endl;

    return 0;
}

