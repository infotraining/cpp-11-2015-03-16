#include <iostream>
#include <memory>

using namespace std;

class Base {
public:
    virtual void doWork() final
    {
        cout << "Base::doWork" << endl;
    }

    virtual void mf1() const
    {
        cout << "Base::mf1" << endl;
    }

    virtual void mf2(const int& a)
    {
        cout << "Base::mf2" << endl;
    }
};

class Derived : public Base
{
public:
    void mf1() const override
    {
        cout << "Derived::mf1" << endl;
    }

    void mf2(const int &a) override
    {
        cout << "Derived::mf2" << endl;
    }

    void doWork()
    {

    }
};

int main()
{
    unique_ptr<Base> pb(new Derived);
    pb->mf1();
    pb->mf2(10);
    return 0;
}

