#include <iostream>
#include <future>
#include <random>
#include <vector>

using namespace std;

long calc_pi(long N)
{
    random_device rd;
    mt19937_64 mt(rd());
    uniform_real_distribution<> dis(-1, 1);

    long counter{};
    for (int i = 0 ; i < N ; ++i)
    {
        double x = dis(mt);
        double y = dis(mt);
        if (x*x + y*y < 1) ++counter;
    }
    return counter;
}

future<double> give_me_pi(long N)
{
    vector<shared_future<long>> results;
    for (int i = 0 ; i < 4 ; ++i)
    {
        results.push_back( async(launch::async, calc_pi, N/4));
    }

    future<double> res = async(launch::deferred,
    [results, N]() mutable {
        long counter{};
        for ( auto &fut : results)
            counter += fut.get();
        return (double)counter/N * 4;
    });
    return res;

}

int main()
{
    long N = 100000000;
    auto start = chrono::high_resolution_clock::now();
    long counter = calc_pi(N);
    cout << "cores? = " << thread::hardware_concurrency() << endl;
    cout << "Pi = " << double(counter)/N * 4 << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    // multithread
    start = chrono::high_resolution_clock::now();

    future<double> res = give_me_pi(N);
    cout << "Pi = " << res.get() << endl;

    end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    return 0;
}

