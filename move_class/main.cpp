#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class Array
{
    int* arr_{nullptr};
    size_t size_{0};
public:
    Array(size_t size, int initial_val) :
        size_(size), arr_(new int[size])
    {
        cout << "ctor" << endl;
        if(size > 0)
          std::fill(arr_, arr_+size_, initial_val);
    }

    Array(const Array& lval)
    {
        cout << "copy ctor" << endl;
        size_ = lval.size_;
        arr_ = new int[size_];
        for(int i = 0 ; i < size_ ; ++i)
        {
            arr_[i] = lval.arr_[i];
        }
    }

    Array(Array&& rval) noexcept // move constructor
    {
        cout << "move ctor" << endl;
        arr_ = rval.arr_;
        size_ = rval.size_;
        rval.size_ = 0;
        rval.arr_ = nullptr;
    }

    Array& operator=(const Array&) = delete;
    Array& operator=(Array&& rval)
    {
        cout << "move operator=" << endl;
        if(this != &rval)
        {
            delete[] arr_;
            arr_ = rval.arr_;
            size_ = rval.size_;
            rval.size_ = 0;
            rval.arr_ = nullptr;
        }
        return *this;
    }

    void print()
    {
        for (size_t i = 0 ; i < size_ ; ++i)
        {
            cout << arr_[i] << ", ";

        }
        cout << endl;
    }

    ~Array()
    {
        cout << "dtor" << endl;
        delete[] arr_;
    }
};

Array generate_array()
{
    return Array(10, -3);
}

int main()
{
    Array arr(10, -1);
    arr = Array(10, -1);

//    Array arr2(5, -2);
//    arr.print();
//    arr2.print();
    Array arr3 = generate_array();
//    arr3.print();
    vector<Array> vec;
    vec.push_back(Array(3,-10));
    vec.push_back(Array(4,-1));
    vec.push_back(Array(5,-2));
//    vec.emplace_back(3, -10);
//    vec.emplace_back(4, -1);
//    vec.emplace_back(2, -2);
    return 0;
}

