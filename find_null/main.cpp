#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Widget
{
    int id_;
public:
    Widget(int id) : id_(id) {}
    void print()
    {
        cout << "Widget with id " << id_ << endl;
    }
};

template<typename Cont>
auto find_null(const Cont& c) -> decltype(std::begin(c))
{
    // returns first nullptr iterator, or end iterator
//    for (auto it = c.cbegin() ; it != c.cend() ; ++it)
//        if (*it == nullptr)
//            return it;
//    return c.cend();
    return find(std::begin(c), std::end(c), nullptr);
}

int main()
{
    //vector<Widget*> vec{ new Widget(1), new Widget(2), nullptr };
    Widget* vec[] = { new Widget(1), new Widget(2), nullptr };
//    for(auto& el : vec)
//    {
//        el->print();
//    }

//    for (vector<Widget*>::const_iterator it = vec.begin();
//         it != vec.end() ; ++it)
//    {
//        //nothing
//    }

//    for (auto it = vec.cbegin();
//         it != vec.end() ; ++it)
//    {
//        //nothing
//    }


    for (auto it = begin(vec); it != find_null(vec) ; ++it)
    {
        (*it)->print();
    }
    return 0;
}

