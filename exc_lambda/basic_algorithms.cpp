#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    //std::mt19937 mt {rd()};
    std::mt19937_64 mt {42};
    std::uniform_int_distribution<int> uniform_dist {1, 42};

    auto gen = [&] () { return uniform_dist(mt); };
    generate(vec.begin(), vec.end(), gen );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    auto even = [] (int arg) { return arg % 2 == 0;};

    // 1a - wyświetl parzyste
    cout << "even: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout,", "),
            even);

    // 1b - wyswietl ile jest parzystych
    cout << endl;
    cout << "even count = " << std::count_if(vec.begin(), vec.end(),
                                             even);
    cout << endl;

    int eliminators[] = {3,5,7};

        // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    auto garbage_start = std::remove_if(vec.begin(), vec.end(),
                                        [&eliminators] (int x)  {
                return std::any_of(begin(eliminators), end(eliminators),
                            [x] (int div){ return x % div == 0; });
    });
    vec.erase(garbage_start, vec.end());

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;


    // 3 - tranformacja: podnieś liczby do kwadratu
    std::transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x*x;} );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 4 - wypisz 5 najwiekszych liczb
    std::nth_element(vec.begin(), vec.begin()+5, vec.end(), greater<int>());
    cout << "5 greatest" << endl;
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, ", "));
    cout << endl;

    // 5 - policz wartosc srednia
    long counter {};
    std::for_each(vec.begin(), vec.end(), [&counter] (int x) { counter+=x;} );
    double avg = double(counter)/vec.size();
    cout << "avg = " << avg << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    vector<int> ls_eq_avg;
    vector<int> gt_avg;

    std::partition_copy(vec.begin(), vec.end(),
                        back_inserter(ls_eq_avg),back_inserter(gt_avg),
                        [avg](int x) { return x <= avg;});

    cout << "ls_eq_avg: ";
    for(const auto& item : ls_eq_avg)
        cout << item <<" ";
    cout << endl;

    cout << "gt_avg: ";
    for(const auto& item : gt_avg)
        cout << item <<" ";
    cout << endl;

}
