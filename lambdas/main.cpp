#include <iostream>
#include <boost/type_index.hpp>
#include <functional>


using namespace std;

void do_something(void(*f)())
{
    f();
}

template<typename F>
void do_something_with_template(F f)
{
    f();
}

void do_something_with_fun(std::function<void()> f)
{
    f();
}

int fun(int a)
{
    return a*a;
}

void fun2()
{
    cout << "Doing something as a function" << endl;
}

int main()
{
    cout << fun(10) << endl;

    auto f1 = [] (int id) {cout << "Lambda " << id << endl;};
    //std::function<void()> f2 = [] { return 42; };
    auto f2 = + [] { return 42; };
    f1(10);
    cout << f2() << endl;

    std::function<void(int)> f;
    void(*pf)(int);
    if (!f)
    {
        cout << "Empty function object" << endl;
        //f(10); // std::bad_function_call
    }

    f = f1;
    pf = f1;
    f(20);
    pf(30);

    int a = 10;
    auto f3 = [a] (int id) { cout << "captured a = " << a << endl; };
    a = 100;
    f3(1);
    //pf = f3;
    f = f3;
    f(1);

    cout << boost::typeindex::type_id_with_cvr<decltype(f1)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(f2)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(f3)>().pretty_name() << endl;

    // do something

    do_something(fun2);

    do_something([] { cout << "doing with lambda" << endl;});
    //do_something([a] { cout << "doing with lambda with capture " << a << endl;});

    int b = 10;

    do_something_with_template([a] {
        cout << "doing with lambda with capture " << a << endl;});

    do_something_with_template([b] {
        cout << "doing with lambda with another capture " << b << endl;});

    do_something_with_fun([b] {
        cout << "doing with lambda with another capture " << b << endl;});
    return 0;
}

