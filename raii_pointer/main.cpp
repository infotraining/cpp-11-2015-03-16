#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Widget
{
public:
    Widget()
    {
        cout << "Widget ctor" << endl;
    }
    ~Widget()
    {
        cout << "Widget dtor" << endl;
    }
    void use()
    {
        cout << "Widget using" << endl;
    }
};

template<typename T>
class raii_ptr
{
    T* ptr_;
public:
    raii_ptr(T* ptr) : ptr_(ptr)
    {

    }

    raii_ptr(const raii_ptr &) = delete;
    raii_ptr& operator=(const raii_ptr&) = delete;

    raii_ptr(raii_ptr&& rval)
    {
        ptr_ = rval.ptr_;
        rval.ptr_ = nullptr; // key point
    }

    raii_ptr& operator=(raii_ptr&& rval)
    {
        if (this != &rval)
        {
            delete ptr_;
            ptr_ = rval.ptr_;
            rval.ptr_ = nullptr;
        }
        return *this;
    }

    ~raii_ptr()
    {
        delete ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    T operator *() const
    {
        return *ptr_;
    }
};

raii_ptr<Widget> generate_widget()
{
    auto ptr = raii_ptr<Widget>(new Widget);
    return ptr;
}

int main()
{    
    raii_ptr<Widget> p(new Widget);
    p->use();
    auto ptr = generate_widget();
    ptr->use();
    vector<raii_ptr<Widget>> rvector;
    rvector.emplace_back(new Widget);
    rvector.emplace_back(new Widget);
    for (auto &el : rvector)
        el->use();
    return 0;
}

