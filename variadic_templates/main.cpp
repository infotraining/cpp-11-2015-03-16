#include <iostream>
#include <tuple>

using namespace std;

template<typename F, typename... A>
void call(F f, A&&... arg)
{
    f(forward<A>(arg)...);
}

void fun(int a)
{
    cout << "A = " << a << endl;
}

void fun2(int a, int b)
{
    cout << "A = " << a << endl;
    cout << "B = " << b << endl;
}

template<typename T>
T add(T x)
{
    return x;
}

template<typename T, typename... Rest>
T add(T first, Rest... args)
{
    return first + add(args...);
}

int main()
{
    call(fun, 10);
    call([] () { cout << "Lambda" << endl;});
    call([] (int par) { cout << "PAR " << par << endl;}, 10);
    call(fun2, 10, 20);
    call([] (int a, int b, int c)
            { cout << a*b*c << endl; },
            10, 20, 30);

    cout << add(2,3,4) << endl;
    cout << add(2,3,4,5) << endl;
    cout << add(2,3,4,5,6) << endl;

    std::tuple<int, double, string> t(1, 3.14, "ala");
    cout << get<2>(t) << endl;

    return 0;
}

