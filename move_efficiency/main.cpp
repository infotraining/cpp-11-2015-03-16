#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <chrono>
#include <cstring>
#include <algorithm>

using namespace std;

class ExpensiveObject
{
private:
    char* data_;
    size_t size_;
public:
    ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
    {
        data_ = new char[size_+1];
        std::strcpy(data_, data);
    }

    ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
    {
        data_ = new char[size_+1];
        std::strcpy(data_, source.data_);
    }

    ExpensiveObject(ExpensiveObject&& rsource) noexcept :
        data_(rsource.data_), size_(rsource.size_)
    {
        rsource.data_ = nullptr;
        rsource.size_ = 0;
    }

    ~ExpensiveObject()
    {
        delete [] data_;
    }

    ExpensiveObject& operator=(const ExpensiveObject& source)
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    void swap(ExpensiveObject& other)
    {
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    const char* data() const
    {
        return data_;
    }

    size_t size() const
    {
        return size_;
    }

    bool operator<(const ExpensiveObject& other) const
    {
        if (std::strcmp(data_, other.data_) < 0)
            return true;
        return false;
    }
};

template<typename Cont>
void sort(Cont& c)
{
    sort(begin(c), end(c));
}

template<typename T>
void sort(list<T>& c)
{
    c.sort();
}

ExpensiveObject expensive_object_generator()
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::random_shuffle(txt, txt + size);

    return ExpensiveObject(txt);
}


template<typename Cont>
void test(Cont c, size_t size)
{
    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < size ; ++i)
    {
        c.push_back(expensive_object_generator());
    }

    auto end1 = chrono::high_resolution_clock::now();
    cout << "Creating, size " << size << " = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end1-start).count();
    cout << " us" << endl;

    sort(c);

    auto end2 = chrono::high_resolution_clock::now();
    cout << "Sorting, size " << size << " = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end2-end1).count();
    cout << " us" << endl;
}



int main()
{
    // create vector, list, deque 10000 ;
    // and try to sort it
    // and time it

    cout << "Vector" << endl;
    vector<ExpensiveObject> vec;
    for (long i = 10000 ; i < 1000001 ; i*= 10)
    {
        test(vec, i);
        vec.clear();
    }

    cout << "List" << endl;
    list<ExpensiveObject> l;
    for (long i = 10000 ; i < 1000001 ; i*= 10)
    {
        test(l, i);
        l.clear();
    }

    cout << "Deque" << endl;
    deque<ExpensiveObject> d;
    for (long i = 10000 ; i < 1000001 ; i*= 10)
    {
        test(d, i);
        d.clear();
    }
    return 0;
}

