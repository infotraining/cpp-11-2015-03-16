#include <iostream>
#include <vector>
#include <future>
#include <atomic>
#include <mutex>

using namespace std;

atomic<long> at_counter{0};
long counter{0};
mutex mtx;

void increase_atom()
{
    for (int i = 0 ; i < 100000 ; ++i)
        at_counter.fetch_add(1);
}

void increase()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        ++counter;
        //if (counter == 1000) return;
    }
}

int main()
{
    vector<future<void>> vec;
    vec.push_back(async(launch::async, increase));
    vec.push_back(async(launch::async, increase));

    for (auto& el : vec) el.wait();

    cout << "Counter = " << counter << endl;
    return 0;
}

