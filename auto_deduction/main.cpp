#include <iostream>
#include <boost/type_index.hpp>
#include <vector>
using namespace std;

template<typename T>
void check_type(const T& obj)
{
    obj;
}

class Widget
{

};

int result()
{
    return 42;
}

int main()
{
    cout << "Hello auto!" << endl;

    int a; // so C style
    int b = 0;
    //Widget w;

    auto ai = 0u; // C++11 style // ai = unsigned int
    static_assert(is_same<unsigned int, decltype(ai)>::value, "Error");
    cout << boost::typeindex::type_id_with_cvr<decltype(ai)>().pretty_name() << endl;

    // C++11 braces initialization
    auto c = int{1};
    int d{1};
    int e = {1};

    double res = result();
    auto res2 = static_cast<double>(result());

    //Widget w(); // function declaration
    Widget w{};  // default constructor
    long l{};    // initialized

    cout << "a = " << a << endl;

    int f{1};
    const int& g = f;
    auto h = g;
    const auto& i = g;
    cout << "g type = " << boost::typeindex::type_id_with_cvr<decltype(g)>().pretty_name() << endl;
    cout << "h type = " << boost::typeindex::type_id_with_cvr<decltype(h)>().pretty_name() << endl;
    cout << "i type = " << boost::typeindex::type_id_with_cvr<decltype(i)>().pretty_name() << endl;


    int tab[5] = {1,2,3,4,5};

    for (auto el : tab)
    {
        el += 2;  // correct, but does not modify table
        cout << el << ", ";
    }

    for (auto& el : tab)
    {
        el += 2;
        cout << el << ", ";
    }
    cout << endl;

    for (const auto& el : tab)
    {
        cout << el << ", ";
    }
    cout << endl;

    for(auto it = begin(tab); it != end(tab); ++it)
    {
        auto& el = *it;
        // whatever
    }

    vector<bool> v{true, false};
    for(auto&& el : v)
    {
        cout << el << ", ";
    }
    cout << endl;

    return 0;
}

