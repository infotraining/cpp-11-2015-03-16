#include <iostream>
#include <set>
#include <vector>
#include <functional>

using namespace std;

class Widget
{
    int id_;
public:
    Widget(int id) : id_(id) {}
    int get_id() const
    {
        return id_;
    }
};

auto main() -> int
{
//    vector<Widget> w;
//    w.emplace_back(1);
//    w.emplace_back(2);

    auto comp = [] (const auto& w1, const auto& w2) // C++14
    {
         return w1.get_id() < w2.get_id();
    };

    set<Widget, decltype(comp)> w(comp);
    w.emplace(20);
    w.emplace(1);
    w.emplace(2);
    w.emplace(10);
    for (auto& el : w)
    {
        cout << el.get_id() << endl;
    }

    vector<int> vec = {1,2,3,4,5};

    auto l1 = [vec] ()  mutable -> vector<int>
    {
        for (auto& el : vec)
            el += 2;
        return vec;
    };
    vector<int> res = l1();

    for (auto& el : vec)
        cout << el << ", ";
    cout << endl;

    for (auto& el : res)
        cout << el << ", ";
    cout << endl;


    return 0;
}


