#include <iostream>
#include <future>
#include <vector>
#include <thread>

using namespace std;

int question(int id)
{
    this_thread::sleep_for(chrono::seconds(1));
    if (id == 13) throw std::logic_error("bad luck");
    return 42 * id;
}

future<int> make_future()
{
    return async(launch::async, question, 2);
}

int main()
{
    cout << "Hello World!" << endl;
    //cout << question() << endl;

    future<int> res = async(launch::async, question, 10);
    auto res2 = make_future();

    cout << "after launching" << endl;

    cout << "result = " << res.get() << endl;
    cout << "result2 = " << res2.get() << endl;

    vector<future<int>> vf;
    for (int i = 0 ; i < 10 ; ++i)
        vf.push_back( async(launch::async, question, i) );

    for (auto & el : vf)
        cout << el.get() << endl;

    future<int> res3 = async(launch::async,question,13);
    res3.wait();

    try
    {
        cout << "res3 = " << res3.get() << endl;
    }

    catch(...)
    {
        cerr << "error" << endl;
    }

    return 0;
}

