#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class Test
{
public:
    Test()
    {
        cout << "Ctor" << endl;
    }
    Test(const Test&) = default;
    Test& operator=(const Test&) = default;

    Test(Test&&) = delete;
    Test& operator=(Test&&) = delete;
};

class Widget
{
    int id{};
    unique_ptr<int> p_;

public:
    Widget(int id) : id(id)
    {
        cout << "widget ctor " << id << endl;
    }
    Widget(int id, unique_ptr<int> p) :
        id(id), p_(move(p))
    {
        cout << "widget + unique ctor " << id << endl;
    }

    Widget() : Widget(0) {}
    Widget(const Widget& w)
    {
        id = w.id;
        cout << "widget cctor " << id << endl;
    }

    Widget(Widget&& w)
    {
        id = w.id;
        cout << "widget mvctor " << id << endl;
    }

    ~Widget()
    {
        cout << "widget dtor " << id << endl;
    }

    void print()
    {
        cout << "widget " << id << endl;
    }

};

class SuperWidget : public Widget
{

};

unique_ptr<Widget> make_widget(int id)
{
    return unique_ptr<Widget>(new Widget{id});
}

void process(const unique_ptr<Widget>& w) // does not control lifetime
{
    if(w)
        w->print();
}

void sink(unique_ptr<Widget> w)  // non-copyable
{
    if(w)
        w->print();
}

template<typename T, typename... A>
unique_ptr<T> make_unique(A&&... arg)
{
    return unique_ptr<T>(new T(forward<A>(arg)...)); // conditional_rvalue_cast
}

int fun_2(int&& a)
{
    return a;
}

int fun(int&& a)
{
    fun_2(move(a)); // must move, because a inside fun is lvalue...!!
    return a;
}

int main()
{
    cout << "unique ptr" << endl;
    vector<Widget*> vec_ptr;
    //vec_ptr.push_back(new Widget{1});

    vector<unique_ptr<Widget>> vec;
    vec.emplace_back(new Widget{2}); //, my_function());
    vec.emplace_back(new SuperWidget{});
    vec.push_back(unique_ptr<Widget>(new Widget{3}));
    vec.push_back(make_unique<Widget>(-1));
    string s("ala");
    vec.push_back(make_unique<Widget>(-2, make_unique<int>(10)));
    cout << "shulb be ala = " << s << endl;
    vec.push_back(make_unique<SuperWidget>()); //, my_function());

    // rvalue
    vec.push_back(make_widget(4));

    // lvalue
    auto ptr = make_widget(5);
    vec.push_back(move(ptr));  // casting to r-value
    if(ptr)
        ptr->print();

    auto ptr2 = make_widget(5);
    process(ptr2);

    sink(move(ptr2));  // explicit conversion
    sink(make_widget(6));

    auto ptr3 = make_widget(7);

    auto l = [ptr3=move(ptr3)] () mutable { sink(move(ptr3)); }; // sort of bind C++14 only
    l();
    //l(); // forbidden

    //std::function<void()> fptr = l; // std::function only binds copyable objects

    //Test t;
    //Test&& rt = move(t);

    return 0;
}

