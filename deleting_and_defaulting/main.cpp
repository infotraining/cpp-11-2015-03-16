#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Widget
{
    int id;
    string name {"noname"};
public:
    Widget(int id) : id(id) {}
    // non-copyable
    Widget(const Widget&) = delete;
    Widget& operator=(const Widget&) = delete;
    // but moveable
    Widget(Widget&& rv) = default;
//    {
//        id = move(rv.id);
//        name = move(rv.name);
//    }

    Widget& operator=(Widget&&) = default;

    ~Widget()
    {
        cout << "dtor" << endl;
    }
};

bool is_odd(int n)
{
    return n%2;
}

bool is_odd(char) = delete;
bool is_odd(double) = delete;

int main()
{
    vector<Widget> v;
    v.push_back(Widget{10});
    cout << "Hello World!" << endl;
    cout << "is 2 odd " << is_odd(2) << endl;
    cout << "is 3 odd " << is_odd(3) << endl;
    cout << "is 3.14 odd " << is_odd(static_cast<int>(3.14)) << endl;
    //cout << "is a odd " << is_odd('a') << endl;
    return 0;
}

