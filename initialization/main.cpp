#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <array>
#include <atomic>
#include <boost/type_index.hpp>

using namespace std;

template<typename Cont>
void print(const Cont& c)
{
    for (auto& el : c)
        cout << el << ", ";
    cout << endl;
}

class Container
{
    vector<int> v_;
public:
    Container()
    {
        cout << "default constructor" << endl;
    }

    Container(int x)
    {
        cout << "Ctor with " << x << endl;
    }

    Container(std::initializer_list<int> l) : v_(std::move(l))
    {
        cout << "std:init ctor" << endl;
        print(v_);
    }
};

int main()
{
    cout << "Hello World!" << endl;

    // C++11 braces initialization
    auto c = int{1};
    int d{1};
    int e = {1};

    std::atomic<int> atom{0};
    //std::atomic<int> fail = 0; does not compile

    int tab[3] = {1,2,3};  // C
    std::array<int, 3> arr{1,2,3};

    vector<double> v{1.0, 2.0, 3.0};

    print(v);

    map<int,string> m = { {1,"one"}, {2, "twp"}};
    //Container con(); // wrong
    Container con{42};
    Container con2{};

    auto list = {1,2,3}; // std::initializer_list<int>, not int[3]

    cout << boost::typeindex::type_id_with_cvr<decltype(list)>().pretty_name() << endl;

    cout << "**************" << endl;

    Container con3{};  // default ctor
    Container con4{1}; // std::initializer_list
    Container con5(5); // int constructor

    vector<int> v2{10,20};
    vector<int> v3(10,20);
    vector<double> v4{10, 19.5};

    auto ptr = make_shared<vector<int>>(10,42);

    print(v2);
    print(v3);
    print(v4);
    print(*ptr);

}

