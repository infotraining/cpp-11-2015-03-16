#include <iostream>
#include <memory>
#include <vector>
#include <string>

using namespace std;

class Human
{
    string name_ {};
    weak_ptr<Human> partner{};

public:
    Human(string name) : name_(move(name))
    {
        cout << "ctor of " << name_ << endl;
    }
    ~Human()
    {
        cout << "dtor of " << name_ << endl;
    }

    void set_partner(weak_ptr<Human> p)
    {
        partner = p;
    }

    void decr()
    {
        cout << "my name is " << name_ << endl;
        shared_ptr<Human> sp_partner = partner.lock();
        if(sp_partner)
            cout << "and my parter is " << sp_partner->name_ << endl;
    }
};

int peacefull(int a)
{
    return a;
}

int  may_throw()
{
    throw std::out_of_range("bad luck...");
    return 42;
}

int fun(int a, shared_ptr<Human> h)
{
    h->decr();
    return a;
}

int main()
{
    auto h1 = make_shared<Human>("Adam");
    auto h2 = make_shared<Human>("Ewa");
    h1->set_partner(h2);
    h2->set_partner(h1);
    h1->decr();
    h2->decr();

    try
    {
        int a = 0;
        fun(may_throw(), make_shared<Human>("Lilith")); // safety guaranteed
        // cout << a++ << a++ << a++ << endl;
        // fun(peacefull(may_throw()), shared_ptr<Human>(new Human("Lilith")));
        // potentially leaking
    }
    catch(...)
    {
        cerr << "Errorrrr" << endl;
    }

    return 0;
}

