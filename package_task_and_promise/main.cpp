#include <iostream>
#include <future>
#include <vector>
#include <thread>
#include <functional>

using namespace std;

int question(int id)
{
    //this_thread::sleep_for(chrono::seconds(1));
    if (id == 13) throw std::logic_error("bad luck");
    return 42 * id;
}

class my_packaged_task
{
    std::function<int(int)> f_;
    std::promise<int> prom_;
public:
    my_packaged_task(std::function<int(int)> f) :
        f_(f)
    {
    }

    future<int> get_future()
    {
        return prom_.get_future();
    }

    void operator ()(int par)
    {
        try{
            prom_.set_value(f_(par));
        }
        catch(...)
        {
            prom_.set_exception(current_exception());
        }

    }
};

int main()
{
    my_packaged_task pt(question);
    future<int> res = pt.get_future();
    pt(10);
//    thread th(move(pt),10);
//    th.detach();
    cout << "res = " << res.get() << endl;
    return 0;
}

