#include <iostream>

using namespace std;

enum class Coffee : unsigned char {espresso, cappucino, latte};

int main()
{
    Coffee my_coffee = Coffee::latte;
    //int a = my_coffee; // does not compile
    int a = static_cast<int>(my_coffee);
    cout << "a = " << a << endl;

    switch (my_coffee) {
    case Coffee::cappucino:
        cout << "cappucino" << endl;
        break;
    case Coffee::espresso:
        cout << "espressp" << endl;
        break;
    case Coffee::latte:
        cout << "latte" << endl;
        break;
    default:
        cout << "brown water :)" << endl;
        break;
    }

    return 0;
}

