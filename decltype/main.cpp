#include <iostream>
#include <boost/type_index.hpp>

using namespace std;

template<typename A, typename B>
auto add(A a, B b) -> decltype(a+b)
{
    return a+b;
}

template<typename A, typename B>
decltype(auto) add2(A a, B b)
{
    return a+b;
}

int main()
{
    int a = 10;

    decltype(a + 10.10) b = a;

    cout << add2(3.14, 4.0f) << endl;

    cout << boost::typeindex::type_id_with_cvr<decltype(b)>().pretty_name() << endl;


    return 0;
}

