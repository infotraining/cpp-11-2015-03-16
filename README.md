## Nowy standard - C++11

### Additonal information

#### login and password for VM:

```
dev  /  tymczasowe
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### GIT

```
git clone https://bitbucket.org/infotraining/...
```

#### Links

* [training materials](http://infotraining.bitbucket.org/cpp-thd/)

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [unique_ptr info](http://stackoverflow.com/questions/8114276/how-do-i-pass-a-unique-ptr-argument-to-a-constructor-or-a-function)

* [memory](http://marek.vavrusa.com/c/memory/2015/02/20/memory/)