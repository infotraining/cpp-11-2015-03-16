#include <iostream>
#include <queue>
#include <functional>

using namespace std;

namespace old_std {

class Task
{
public:
    virtual void operator()()
    {

    }
    virtual ~Task() {}
};

class MyTask : public Task
{
public:
    void operator()() override
    {
        cout << "My task " << endl;
    }
};

queue<Task*> q;

void produce_task(Task* task)
{
    q.push(task);
}

void execute()
{
    while(!q.empty())
    {
        (*q.front())();
        delete q.front();
        q.pop();
    }
}

}

namespace new_std
{

using task_t = std::function<void()>;

queue<task_t> q;

void push_task(task_t task)
{
    q.push(std::move(task));
}

void execute()
{
    while(!q.empty())
    {
        task_t task = q.front();
        if(task)
            task();
        q.pop();
    }
}

}

using namespace new_std;

void my_task()
{
    cout << "my function is a task" << endl;
}

void dangler()
{
    int a = 10;
    q.push([&a] { cout << "value of " << a << endl;});

}

int main()
{
    for(int i = 0 ; i < 10 ; ++i)
        push_task([i] {cout << "new task " << i << endl;});
    push_task(my_task);
    //push_task(nullptr);
    //dangler();
    execute();
    return 0;
}

