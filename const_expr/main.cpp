#include <iostream>
#include <array>

using namespace std;

constexpr int cube(int n)
{
    return n*n*n;
}

constexpr size_t factorial(size_t n)
{
    return (n == 0) ? 1 : n * factorial(n-1);
}

int main()
{
    //const int a = 3;
    array<int, factorial(3)> arr;
    cout << "10 ^ 3 = " << cube(10) << endl;
    constexpr size_t fact_10 = factorial(10);
    cout << fact_10 << endl;
    return 0;
}

