#include <iostream>
#include <memory>

using namespace std;

void f_ptr(int* ptr)
{
    cout << "fptr with ptr" << endl;
    if(ptr)
    {
        cout << "value = " << *ptr << endl;h
    }
    else
    {
        cout << "nullptr" << endl;
    }
}

void f_ptr(int a)
{
    cout << "fptr with int" << endl;
}

void f_ptr(nullptr_t ptr)
{
    cout << "nullptr version" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    int a = 10;
    f_ptr(&a);
    f_ptr(static_cast<int>(NULL));
    //f_ptr(NULL);
    f_ptr(nullptr);
    return 0;
}

